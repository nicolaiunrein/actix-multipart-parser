use actix_multipart::Multipart;
use actix_multipart_parser::{parse, MultipartFormData};
use actix_web::{middleware, web, App, HttpResponse, HttpServer};
use std::convert::{From, Into};

#[derive(Debug, Default)]
struct MyRequest {
    pub ip: Option<String>,
    pub port: Option<u16>,
    pub level: Option<u8>,
}

impl From<MultipartFormData> for MyRequest {
    fn from(mut payload: MultipartFormData) -> Self {
        Self {
            ip: payload.texts.remove("ip").and_then(|mut texts| texts.pop()),
            port: payload
                .texts
                .remove("port")
                .and_then(|mut texts| texts.pop())
                .and_then(|port| port.parse().ok()),
            level: payload
                .texts
                .remove("level")
                .and_then(|mut texts| texts.pop())
                .and_then(|level| level.parse().ok()),
        }
    }
}

async fn handle_form(payload: Multipart) -> Result<HttpResponse, HttpResponse> {
    let request: MyRequest = parse(payload).await.unwrap_or_default().into();

    println!("{:?}", request);

    Ok(HttpResponse::Ok().into())
}

fn index() -> HttpResponse {
    let html = r#"<html>
        <head><title>Ip/Port Test</title></head>
        <body>
            <form target="/" method="post" enctype="multipart/form-data">
                <label for="ip">IP</label>
                <input type="text" name="ip"/>
                <br />
                <label for="port">Port</label>
                <input type="text" name="port"/>
                <input type="range" name="level" min="0" max="255" value="127"/>
                <input type="submit" value="Submit"></button>
            </form>
        </body>
    </html>"#;

    HttpResponse::Ok().body(html)
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    std::env::set_var("RUST_LOG", "actix_server=info,actix_web=info");
    std::fs::create_dir_all("./my_uploads").unwrap();

    let ip = "0.0.0.0:3000";

    HttpServer::new(|| {
        App::new().wrap(middleware::Logger::default()).service(
            web::resource("/")
                .route(web::get().to(index))
                .route(web::post().to(handle_form)),
        )
    })
    .bind(ip)?
    .start()
    .await
}
