Actix Multipart Parser
====================

This crate provides a multipart/form-data parser for the actix-web framework.
It is inspired by the Rocket-Multipart-Form-Data crate and builds on top of the actix-multipart crate.

## Example

```rust
use actix_multipart::Multipart;
use actix_multipart_parser::{parse, Error};
use actix_web::{middleware, web, App, HttpResponse, HttpServer};
use futures::TryFutureExt;

// a simple multipart/form-data handler for demonstration
async fn handle_form(payload: Multipart) -> Result<HttpResponse, Error> {
    let mut data = parse(payload).await?;

    // get a Vec<String> of the values of all parts with name="multi-text"
    let multi_text = data.texts.remove("multi-text").unwrap_or_default();

    println!("Multiple text fields with same name: {:#?}", multi_text);

    // get only the first value for the given name="single-text"
    let single_text = data.texts.remove("single-text").unwrap_or_default().pop();

    println!("Single Text Field: {:#?}", single_text);

    // get a vec of uploaded files
    let mut files = data.files.remove("upload").unwrap_or_default();

    println!("Files: {:#?}", files);

    // loop over received files and persist to local directory.
    while let Some(f) = files.pop() {
        // filesystem operations are blocking, we have to use threadpool
        web::block(move || f.file.persist(format!("./my_uploads/{}", f.filename)))
            .map_err(|e| Error::IoError(e.to_string()))
            .await?;
    }

    Ok(HttpResponse::Ok().into())
}

// create/serve a simple form to play with
fn index() -> HttpResponse {
    let html = r#"<html>
        <head><title>Upload Test</title></head>
        <body>
            <form target="/" method="post" enctype="multipart/form-data">
                <label for="single-text" >A single text-field:</label>
                <input type="text" name="single-text" label="Single"/>
                <hr>
                <label for="multi-text" >Two text-fields with the same name:</label>

                <input type="text" name="multi-text"/>
                <input type="text" name="multi-text"/>
                <hr>
                <input type="file" multiple name="upload"/>
                <hr>
                <input type="submit" value="Submit"></button>
            </form>
        </body>
    </html>"#;

    HttpResponse::Ok().body(html)
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    std::env::set_var("RUST_LOG", "actix_server=info,actix_web=info");
    std::fs::create_dir_all("./my_uploads").unwrap();

    let ip = "0.0.0.0:3000";

    HttpServer::new(|| {
        App::new().wrap(middleware::Logger::default()).service(
            web::resource("/")
                .route(web::get().to(index))
                .route(web::post().to(handle_form)),
        )
    })
    .bind(ip)?
    .start()
    .await
}


```

Also see `examples`.

## Crates.io

Not yet published

## Documentation

https://docs.rs/actix-multipart-parser

## License

[MIT](LICENSE)
