#![allow(dead_code)]

use actix_multipart::Multipart;
use actix_web::error::BlockingError;
use actix_web::{web, HttpResponse, ResponseError};
use failure::Fail;
use futures::StreamExt;
use std::collections::HashMap;
use std::io::Read;
use std::io::Write;
use tempfile;
//mod schema;

#[derive(Debug, Default)]
pub struct MultipartFormData {
    pub files: HashMap<String, Vec<FileField>>,
    pub texts: HashMap<String, Vec<String>>,
}

impl MultipartFormData {
    pub fn take_named_tempfile<S: Into<String>>(
        &mut self,
        name: S,
    ) -> Option<tempfile::NamedTempFile> {
        self.files
            .remove(&name.into())
            .unwrap_or_default()
            .pop()
            .map(|field| field.file)
    }

    pub fn take_file<S: Into<String>>(&mut self, name: S) -> Result<std::fs::File, Error> {
        match self.take_named_tempfile(name) {
            None => Err(Error::NoSuchFile),
            Some(f) => Ok(f.reopen()?),
        }
    }

    pub async fn take_file_as_string<S: Into<String>>(&mut self, name: S) -> Result<String, Error> {
        let mut file = self.take_file(name)?;
        let content = web::block(move || -> Result<String, Error> {
            let mut content = String::new();
            file.read_to_string(&mut content)?;
            Ok(content)
        })
        .await
        .map_err(Error::from);
        Ok(content?)
    }
}

#[derive(Debug)]
pub struct FileField {
    pub file: tempfile::NamedTempFile,
    pub filename: String,
}

#[derive(Debug, Fail)]
pub enum Error {
    #[fail(display = "NoContentDisposition")]
    NoContentDisposition,
    #[fail(display = "UnnamedField")]
    UnnamedField,
    #[fail(display = "InvalidPart: {}", 0)]
    InvalidPart(String),
    #[fail(display = "IoError: {}", 0)]
    IoError(String),
    #[fail(display = "NoSuchFile")]
    NoSuchFile,
}

impl From<BlockingError<Error>> for Error {
    fn from(e: BlockingError<Error>) -> Self {
        Self::IoError(e.to_string())
    }
}

impl ResponseError for Error {
    fn error_response(&self) -> HttpResponse {
        match self {
            Self::NoContentDisposition => HttpResponse::BadRequest().finish(),
            Self::UnnamedField => HttpResponse::BadRequest().finish(),
            Self::InvalidPart(e) => HttpResponse::BadRequest().body(e),
            Self::IoError(e) => HttpResponse::InternalServerError().body(e),
            Self::NoSuchFile => HttpResponse::BadRequest().finish(),
        }
    }
}

impl From<std::io::Error> for Error {
    fn from(e: std::io::Error) -> Self {
        Self::IoError(e.to_string())
    }
}

pub async fn parse(payload: Multipart) -> Result<MultipartFormData, Error> {
    parse_in(payload, "/tmp".into()).await
}

pub async fn parse_in(
    mut payload: Multipart,
    dir: std::path::PathBuf,
) -> Result<MultipartFormData, Error> {
    let mut res = MultipartFormData {
        files: HashMap::new(),
        texts: HashMap::new(),
    };
    while let Some(item) = payload.next().await {
        let mut field = item.map_err(|e| Error::InvalidPart(e.to_string()))?;
        let content_type = field
            .content_disposition()
            .ok_or_else(|| Error::NoContentDisposition)?;

        let name = content_type.get_name();
        let filename = content_type.get_filename();

        let _ = match (name, filename) {
            (Some(name), None) => {
                // handle Text Field
                let mut value = String::new();
                while let Some(chunk) = field.next().await {
                    chunk
                        .map_err(|e| Error::InvalidPart(e.to_string()))?
                        .into_iter()
                        .map(|b| b as char)
                        .for_each(|c| value.push(c));
                }
                res.texts.entry(name.to_string()).or_default().push(value);
                Ok(())
            }
            (Some(name), Some(filename)) => {
                //handle File Field
                if !filename.is_empty() {
                    let dir = dir.clone();
                    let mut tmp_file = web::block(move || tempfile::NamedTempFile::new_in(dir))
                        .await
                        .map_err(|e| Error::IoError(e.to_string()))?;

                    //// Field in turn is stream of *Bytes* object
                    while let Some(chunk) = field.next().await {
                        let data = chunk.map_err(|e| Error::InvalidPart(e.to_string()))?;

                        // filesystem operations are blocking, we have to use threadpool
                        tmp_file = web::block(move || tmp_file.write_all(&data).map(|_| tmp_file))
                            .await
                            .map_err(|e| Error::IoError(e.to_string()))?;
                    }

                    res.files
                        .entry(name.to_string())
                        .or_default()
                        .push(FileField {
                            file: tmp_file,
                            filename: filename.to_string(),
                        });
                }
                Ok(())
            }
            _ => Err(Error::UnnamedField),
        }?;
    }

    Ok(res)
}

#[cfg(test)]
mod tests {
    use super::*;
    use actix_http::http::HeaderMap;
    use actix_rt;
    use actix_utils::mpsc;
    use actix_web::error::PayloadError;
    use actix_web::http::header;
    use bytes::Bytes;
    use futures::stream::{Stream, StreamExt};
    use pretty_assertions::assert_eq;

    fn create_stream() -> (
        mpsc::Sender<Result<Bytes, PayloadError>>,
        impl Stream<Item = Result<Bytes, PayloadError>>,
    ) {
        let (tx, rx) = mpsc::channel();

        (tx, rx.map(|res| res.map_err(|_| panic!())))
    }

    fn create_test_headers() -> HeaderMap {
        let mut headers = HeaderMap::new();
        headers.insert(
            header::CONTENT_TYPE,
            header::HeaderValue::from_static(
                "multipart/mixed; boundary=\"abbc761f78ff4d7cb7573b5a23f96ef0\"",
            ),
        );
        headers
    }

    fn create_simple_request() -> (Bytes, HeaderMap) {
        let bytes = Bytes::from(
            "testasdadsad\r\n\
             --abbc761f78ff4d7cb7573b5a23f96ef0\r\n\
             Content-Disposition: form-data; name=\"name\"\r\n\
             Content-Type: text/plain; charset=utf-8\r\nContent-Length: 4\r\n\r\n\
             abcd\r\n\
             --abbc761f78ff4d7cb7573b5a23f96ef0--\r\n",
        );
        let headers = create_test_headers();
        (bytes, headers)
    }

    fn create_simple_request_multiple_fileds_with_same_name() -> (Bytes, HeaderMap) {
        let bytes = Bytes::from(
            "testasdadsad\r\n\
             --abbc761f78ff4d7cb7573b5a23f96ef0\r\n\
             Content-Disposition: form-data; name=\"name\"\r\n\
             Content-Type: text/plain; charset=utf-8\r\nContent-Length: 4\r\n\r\n\
             abcd\r\n\
             --abbc761f78ff4d7cb7573b5a23f96ef0\r\n\
             Content-Disposition: form-data; name=\"name\"\r\n\
             Content-Type: text/plain; charset=utf-8\r\nContent-Length: 3\r\n\r\n\
             xyz\r\n\
             --abbc761f78ff4d7cb7573b5a23f96ef0--\r\n",
        );
        let headers = create_test_headers();
        (bytes, headers)
    }
    fn create_file_request() -> (Bytes, HeaderMap) {
        let bytes = Bytes::from(
            "testasdadsad\r\n\
             --abbc761f78ff4d7cb7573b5a23f96ef0\r\n\
             Content-Disposition: form-data; name=\"test_file\"; filename=\"test_file.txt\"\r\n\
             Content-Type: text/plain; charset=utf-8\r\nContent-Length: 12\r\n\r\n\
             test_content\r\n\
             --abbc761f78ff4d7cb7573b5a23f96ef0--\r\n",
        );
        let headers = create_test_headers();
        (bytes, headers)
    }

    fn create_file_request_multiple_files() -> (Bytes, HeaderMap) {
        let bytes = Bytes::from(
            "testasdadsad\r\n\
             --abbc761f78ff4d7cb7573b5a23f96ef0\r\n\
             Content-Disposition: form-data; name=\"test_file\"; filename=\"test_file1.txt\"\r\n\
             Content-Type: text/plain; charset=utf-8\r\nContent-Length: 13\r\n\r\n\
             test_content1\r\n\
             --abbc761f78ff4d7cb7573b5a23f96ef0\r\n\
             Content-Disposition: form-data; name=\"test_file\"; filename=\"test_file2.txt\"\r\n\
             Content-Type: text/plain; charset=utf-8\r\nContent-Length: 13\r\n\r\n\
             test_content2\r\n\
             --abbc761f78ff4d7cb7573b5a23f96ef0--\r\n",
        );
        let headers = create_test_headers();
        (bytes, headers)
    }

    #[actix_rt::test]
    async fn it_works_with_single_field() {
        let (sender, payload) = create_stream();
        let (bytes, headers) = create_simple_request();
        sender.send(Ok(bytes)).unwrap();

        let multipart_data = Multipart::new(&headers, payload);
        let mut multipart_form_data = parse(multipart_data).await.unwrap();

        //let photos = multipart_form_data.files.get("photo").unwrap();
        let mut names = multipart_form_data.texts.remove("name").unwrap_or_default();

        if let Some(name) = names.pop() {
            if !names.is_empty() {
                panic!("more than one name provided")
            }
            assert_eq!(name, "abcd".to_string());
        } else {
            panic!("no name")
        }
    }

    #[actix_rt::test]
    async fn it_works_with_multi_field() {
        let (sender, payload) = create_stream();
        let (bytes, headers) = create_simple_request_multiple_fileds_with_same_name();
        sender.send(Ok(bytes)).unwrap();

        let multipart_data = Multipart::new(&headers, payload);
        let mut multipart_form_data = parse(multipart_data).await.unwrap();

        //let photos = multipart_form_data.files.get("photo").unwrap();
        let names = multipart_form_data.texts.remove("name").unwrap_or_default();
        assert_eq!(names, vec!["abcd".to_string(), "xyz".to_string()]);
    }

    #[actix_rt::test]
    async fn it_works_with_single_file() {
        let (sender, payload) = create_stream();
        let (bytes, headers) = create_file_request();
        sender.send(Ok(bytes)).unwrap();

        let multipart_data = Multipart::new(&headers, payload);
        let mut multipart_form_data = parse(multipart_data).await.unwrap();

        let files: Vec<(String, String)> = multipart_form_data
            .files
            .remove("test_file")
            .unwrap_or_default()
            .iter()
            .map(|f| {
                (
                    f.filename.clone(),
                    std::fs::read_to_string(f.file.path()).unwrap(),
                )
            })
            .collect();

        assert_eq!(
            files,
            vec![("test_file.txt".to_string(), "test_content".to_string())]
        )
    }
    #[actix_rt::test]
    async fn it_works_with_multiple_files() {
        let (sender, payload) = create_stream();
        let (bytes, headers) = create_file_request_multiple_files();
        sender.send(Ok(bytes)).unwrap();

        let multipart_data = Multipart::new(&headers, payload);
        let mut multipart_form_data = parse(multipart_data).await.unwrap();

        let files: Vec<(String, String)> = multipart_form_data
            .files
            .remove("test_file")
            .unwrap_or_default()
            .iter()
            .map(|f| {
                (
                    f.filename.clone(),
                    std::fs::read_to_string(f.file.path()).unwrap(),
                )
            })
            .collect();

        assert_eq!(
            files,
            vec![
                ("test_file1.txt".to_string(), "test_content1".to_string()),
                ("test_file2.txt".to_string(), "test_content2".to_string())
            ]
        )
    }
}
