use ::serde::Serialize;
use serde_derive;
use serde_json;
use std::collections::HashMap;

pub enum Part {
    SingleText,
    MultiText,
    SingleFile,
    MultiFile,
}

type Schema = HashMap<String, Part>;
type Data = HashMap<String, Vec<String>>;

pub fn to_struct<T>(schema: Schema, data: HashMap<String, Vec<String>>) -> Result<T, ()> {
    serde_json::parse::<T>()
}

#[cfg(test)]
mod test {
    use super::*;

    #[derive(Debug, Serialize, PartialEq)]
    struct Test {
        a: String,
        b: u8,
        c: Vec<f32>,
    }
    #[test]
    fn it_works() {
        let schema = HashMap::new();
        schema.insert("a".to_string(), Part::SingleText);
        schema.insert("b".to_string(), Part::SingleText);
        schema.insert("c".to_string(), Part::MultiText);

        let data = HashMap::new();
        data.insert("a".to_string(), vec!["hello".to_string()]);
        data.insert("b".to_string(), vec!["17".to_string()]);
        data.insert("c".to_string(), vec!["19".to_string(), "32".to_string()]);

        let test = to_struct::<Test>(schema, data).unwrap();

        assert_eq!(
            test,
            Test {
                a: "hello".to_string(),
                b: 17,
                c: vec![19.0, 32.0]
            }
        )
    }
}
